import app from './app'
import initializeDatabase from './db'

const start = async () => {
  const controller = await initializeDatabase();

  app.get("/", (req, res, next) => res.send("ok"));

  // CREATE
  app.get("/candles/new", async (req, res, next) => {
    try {
      const { name, per,smell } = req.query;
      const result = await controller.createCandle({ name, per,smell });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });

  // READ
  app.get("/candles/get/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const candle = await controller.getCandle(id);
      res.json({ success: true, result: candle });
    } catch (e) {
      next(e);
    }
  });

  // DELETE
  app.get("/candles/delete/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.deleteCandle(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });

  // UPDATE
  app.get("/candles/update/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const { name, per,smell } = req.query;
      const result = await controller.updateContact(id, { name,per, smell });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });

  // LIST
  app.get("/candles/list", async (req, res, next) => {
    try {
      const { order } = req.query;
      const candles = await controller.getCandlesList(order);
      res.json({ success: true, result: candles });
    } catch (e) {
      next(e);
    }
  });

  // ERROR
  app.use((err, req, res, next) => {
    console.error(err)
    const message = err.message
    res.status(500).json({ success:false, message })
  })
  
  app.listen(8080, () => console.log("server listening on port 8080"));
};

start();
