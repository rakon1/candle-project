import React, { Component } from "react";
import Candle from "./Candle";
import "./App.css";

class App extends Component {
  state = {
    candle_list: [],
    error_message: "",
    name: "",
    per: "",
    smell:""
  };
  getCandle = async id => {
    // check if we already have the contact
    const previous_candle = this.state.candle_list.find(
      candles => candles.id === id
    );
    if (previous_candle) {
      return; // do nothing, no need to reload a contact we already have
    }
    try {
      const response = await fetch(`http://localhost:8080/candles/get/${id}`);
      const answer = await response.json();
      if (answer.success) {
        // add the user to the current list of contacts
        const candle = answer.result;
        const candle_list = [...this.state.candle_list, candle];
        this.setState({ candle_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  deleteCandle = async id => {
    try {
      const response = await fetch(
        `http://localhost:8080/candles/delete/${id}`
      );
      const answer = await response.json();
      if (answer.success) {
        // remove the user from the current list of users
        const candle_list = this.state.candle_list.filter(
          candles => candles.id !== id
        );
        this.setState({ candle_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  updateCandle = async (id, props) => {
    try {
      if (!props || !(props.name || props.per || props.smell)) {
        throw new Error(
          `you need at least name `
        );
      }
      const response = await fetch(
        `http://localhost:8080/candles/update/${id}?name=${props.name}&per=${props.per}&smell=${props.smell}`
      );
      const answer = await response.json();
      if (answer.success) {
        // we update the user, to reproduce the database changes:
        const candle_list = this.state.candle_list.map(candle => {
          // if this is the contact we need to change, update it. This will apply to exactly
          // one contact
          if (candle.id === id) {
            const new_candle = {
              id: candle.id,
              name: props.name || candle.name,
              per: props.name || candle.per,
              smell: props.name || candle.smell
            };
            return new_candle;
          }
          // otherwise, don't change the contact at all
          else {
            return candle;
          }
        });
        this.setState({ candle_list});
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  createCandle = async props => {
    try {
      if (!props || !(props.name && props.per && props.smell)) {
        throw new Error(
          `errrrrrrrrrrrrrrrrrrrrror `
        );
      }
      const { name, per, smell } = props;
      const response = await fetch(
        `http://localhost:8080/candles/new/?name=${name}&per=${per}&smell=${smell}`
      );
      const answer = await response.json();
      if (answer.success) {
        // we reproduce the user that was created in the database, locally
        const id = answer.result;
        const candle = { name, per,smell, id };
        const candle_list = [...this.state.candle_list, candle];
        this.setState({ candle_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  getCandlesList = async order => {
    try {
      const response = await fetch(
        `//localhost:8080/candles/list`
      );
      const answer = await response.json();
      if (answer.success) {
        const candle_list = answer.result;
        console.log("here",candle_list)
        this.setState({ candle_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };
  async componentDidMount() {
   await this.getCandlesList();
  }
  onSubmit = evt => {
    // stop the form from submitting:
    evt.preventDefault();
    // extract name and email from state
    const { name, per,smell } = this.state;
    // create the contact from mail and email
    this.createCandle({ name, per,smell });
    // empty name and email so the text input fields are reset
    this.setState({ name: "", per: "",smell:"" });
  };
  render() {
    const { candle_list, error_message } = this.state;
    console.log(candle_list);
    return (
      <div className="App">
        {error_message ? <p> ERROR! {error_message}</p> : false}
        {candle_list.map(candle => (
          <Candle
            key={candle.id}
            id={candle.id}
            name={candle.name}
            per={candle.per}
            smell={candle.smell}
            updateCandle={this.updateCandle}
            deleteCandle={this.deleteCandle}
          />
        ))}
        <form className="third" onSubmit={this.onSubmit}>
          <input
            type="text"
            placeholder="name"
            onChange={evt => this.setState({ name: evt.target.value })}
            value={this.state.name}
          />
          <input
            type="text"
            placeholder="per"
            onChange={evt => this.setState({ per: evt.target.value })}
            value={this.state.per}
          />
          <input
            type="text"
            placeholder="smell"
            onChange={evt => this.setState({ smell: evt.target.value })}
            value={this.state.smell}
          />
          <div>
            <input type="submit" value="ok" />
            <input type="reset" value="cancel" className="button" />
          </div>
        </form>
      </div>
    );
  }
}

export default App;
