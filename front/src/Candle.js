import React from 'react'

export default class Candle extends React.Component {
  state = {
    editMode: false
  };
  toggleEditMode = () => {
    const editMode = !this.state.editMode;
    this.setState({ editMode });
  };
  renderEditMode() {
    const { id, name, per,smell, updateCandle, deleteCandle } = this.props;
    return <div> edit mode</div>;
  }
  renderViewMode() {
    const { id, name,per,smell, deleteCandle } = this.props;
    return (
      <div>
        <span>
          {id} - {name}
        </span>
        <button onClick={this.toggleEditMode} className="success">
          edit
        </button>
        <button onClick={() => deleteCandle(id)} className="warning">
          x
        </button>
        
      </div>
    );
  }
  
  renderEditMode() {
    const { name, per,smell } = this.props;
    return (
      
      <form
        className="third"
        onSubmit={this.onSubmit}
        onReset={this.toggleEditMode}
      >
        <input
          type="text"
          placeholder="name"
          name="candle_name_input"
          defaultValue={name}
        />
        <input
          type="text"
          placeholder="per"
          name="candle_per_input"
          defaultValue={per}
        />
        <input
          type="text"
          placeholder="smell"
          name="candle_smell_input"
          defaultValue={smell}
        />
        <div>
          <input type="submit" value="ok" />
          <input type="reset" value="cancel" className="button" />
        </div>
      </form>
    );
  }
  onSubmit = evt => {
    // stop the page from refreshing
    evt.preventDefault();
    // target the form
    const form = evt.target;
    // extract the two inputs from the form
    const candle_name_input = form.candle_name_input;
    const candle_per_input = form.candle_per_input;
    const candle_smell_input = form.candle_smell_input;
    // extract the values
    const name = candle_name_input.value;
    const per = candle_per_input.value;
    const smell = candle_smell_input.value;
    // get the id and the update function from the props
    const { id, updateCandle } = this.props;
    // run the update contact function
    updateCandle(id, { name, per,smell });
    // toggle back view mode
    this.toggleEditMode();
  };
  render() {
    const { editMode } = this.state;
    if (editMode) {
      return this.renderEditMode();
    } else {
      return this.renderViewMode();
      
    }
  }
}
